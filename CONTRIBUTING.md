Reminder: This software is provided as-is and with no warranty.

Bug Reporting and Dispostion:
* The developers provide absolutely no service agreement, expressed or implied.
* Bugs reports must include:
    * a clear statement of the expected behavior
    * a clear statement of the observed behavior
    * a *link* to the commit, tag, or release in gitlab the bug was detected under
* The developers reserve the right to reject or ignore any bug reports.

Code Contribution:
* The developers reserve the right to reject or ignore any code submissions.
* To be considered:
    * Submissions *must* include and pass all unit tests and produce a 100% code coverage for unit tests.
    * Submissions *must* include and pass all Python doc tests and produce a 100% code coverage for library documentation tests.
    * Submissions *must* include and pass README doc tests and produce a 100% code coverage for all README documentation tests.
    * Submissions *must* be in a branch of their own.

(MORE TO COME)
